<?php



//REST API

//get       - get data
//post      - save data 
//patch     - edit data
//delete    - delete data

//POST CONTROLLER (CRUD - Create Read Update Delete)
//GET       /post     - all posts
Route::get('/', 'PostController@Index');

//GET       /post/create
Route::get('/post/create', 'PostController@CreatePost');

//GET       /post/edit
Route::get('/post/edit/{id}', 'PostController@EditPost');

//GET       /post/1   - one post
Route::get('/post/{id}', 'PostController@Details');


//POST      /post     - add new post
Route::post('/post', 'PostController@SavePost');

//PATCH     /post/1   - edit post
Route::patch('/post/{id}', 'PostController@UpdatePost');

//DELETE    /post/1   -delete post     
Route::delete('/post/{id}', 'PostController@DeletePost');
