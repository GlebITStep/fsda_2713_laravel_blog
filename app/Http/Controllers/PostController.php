<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    //GET /
    public function Index()
    {
        $posts = Post::all();
        return view('index', ['posts' => $posts]);
    }

    //GET /post/1
    public function Details($id)
    {
        $post = Post::find($id);
        return view('post.details', ['post' => $post]);
    }

    //GET /post/create
    public function CreatePost()
    {
        return view('post.create');
    }

    //GET /post/edit
    public function EditPost($id)
    {
        $post = Post::find($id);

        return view('post.edit', [ 'post' => $post ]);
    }

     //POST /post
    public function SavePost()
    {
        $post = new Post();

        $post->title = request('title');
        $post->content = request('content');
        
        if (request()->hasFile('image')) 
        {
            $image = request()->file('image');
            $post->image = $this->UploadImage($image);
        }

        $post->save();

        return redirect()->action('PostController@Index');
    }

     //PATCH /post/1
    public function UpdatePost($id)
    {
        $post = Post::find($id);
        
        $post->title = request('title');
        $post->content = request('content');

        if (request()->hasFile('image')) 
        {
            $image = request()->file('image');
            $post->image = $this->UploadImage($image);
        }

        $post->save();

        return redirect()->action('PostController@Index');
    }

     //DELETE /post/1
    public function DeletePost($id)
    {
        Post::find($id)->delete();

        return redirect()->action('PostController@Index');
    }

    private function UploadImage($image)
    {
        $name = time() . '.' . $image->getClientOriginalExtension();
        $path = public_path('/images');
        $image->move($path, $name);
        return $name;
    }
}





// public function SavePost()
// {
//     $post = new Post();
//     $post->title = request('title');
//     $post->content = request('content');
    
//     // if (request()->hasFile('image')) 
//     // {
//     //     $image = request()->file('image');
//     //     $name = time() . '.' . $image->getClientOriginalExtension();
//     //     $path = public_path('/images');
//     //     $image->move($path, $name);            
//     //     $post->image = $name;
//     // }
    
//     if (request()->hasFile('image')) 
//     {          
//         $post->image = $this->UploadImage(request());
//     } 

//     $post->save();

//     return redirect()->action('PostController@Index');
// }

// public function UpdatePost(int $id)
// {
//     Post::find($id)->delete();       
//     return redirect()->action('PostController@Index');
// }

// public function DeletePost(int $id)
// {
//     $post = Post::find($id);
//     $post->title = request('title');
//     $post->content = request('content');

//     // if (request()->hasFile('image')) 
//     // {
//     //     $image = request()->file('image');
//     //     $name = time() . '.' . $image->getClientOriginalExtension();
//     //     $path = public_path('/images');
//     //     $image->move($path, $name);            
//     //     $post->image = $name;
//     // }

//     if (request()->hasFile('image')) 
//     {          
//         $post->image = $this->UploadImage(request());
//     }

//     $post->save();

//     return redirect()->action('PostController@Index');
// }

// private function UploadImage($request) 
// {
//     $image = request()->file('image');
//     $name = time() . '.' . $image->getClientOriginalExtension();
//     $path = public_path('/images');
//     $image->move($path, $name);
//     return $name;
// }