@extends('layout')
@section('title', 'Create a new post')

@section('content')
<h1>Create</h1>

<form method="post" action="/post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Title: </label>
        <input type="text" class="form-control" id="title" name="title">
    </div>
    <div class="form-group">
        <label for="image">Image URL: </label>
        <input type="file" class="form-control-file" id="image" name="image">
    </div>
    <div class="form-group">
        <label for="content">Content: </label>
        <textarea class="form-control" name="content" id="content" rows="3"></textarea>
    </div>
    <button class="btn btn-primary">Create</button>
</form>
@endsection