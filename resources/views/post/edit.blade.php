@extends('layout')
@section('title', 'Edit post')

@section('content')
<h1>Edit</h1>

<form method="post" action="/post/{{$post->id}}" enctype="multipart/form-data">
    @csrf
    @method('patch')
    <div class="form-group">
        <label for="title">Title: </label>
        <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
    </div>
    <div class="form-group">
        <label for="image">Image URL: </label>
        <input type="file" class="form-control-file" id="image" name="image">
    </div>
    <div class="form-group">
        <label for="content">Content: </label>
        <textarea class="form-control" name="content" id="content" rows="3">{{$post->content}}</textarea>
    </div>
    <button class="btn btn-primary">Edit</button>
</form>
@endsection