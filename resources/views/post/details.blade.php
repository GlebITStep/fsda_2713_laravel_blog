@extends('layout')

@section('title', 'Post')

@section('content')
    <img src="/images/{{$post->image}}" class="w-100" height="300" style="object-fit:cover" alt="">
    <h1>{{$post->title}}</h1>
    <div>{!! $post->content !!}</div>
@endsection