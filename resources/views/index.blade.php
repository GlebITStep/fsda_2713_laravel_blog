@extends('layout')

@section('name', 'Home')

@section('content')
<h1>Index</h1>

<div class="row">
    @foreach ($posts as $item)
    <div class="col-md-4 my-3">
        <div class="card">
            <img src="/images/{{$item->image}}" height="200" style="object-fit:cover" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{$item->title}}</h5>
                <p class="card-text">{{$item->content}}</p>
                <a href="/post/{{$item->id}}" class="btn btn-primary">Read more...</a>
                <button data-id="{{$item->id}}" type="button" class="btn btn-danger delete-btn" data-toggle="modal" data-target="#deleteModal">
                    <i class="fas fa-trash-alt"></i>
                </button>
                <a href="/post/edit/{{$item->id}}" class="btn btn-secondary"><i class="fas fa-edit"></i></a>
            </div>
        </div>
    </div>
    @endforeach
</div>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form id="delete-form" method="post" class="d-inline">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('.delete-btn').click(function() {
        let id = $(this).data('id');
        $('#delete-form').attr('action', `/post/${id}`);
    });
</script>
@endsection